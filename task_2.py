import collections
import re


def most_common_words(text: str):
    text = text.lower()
    pattern = re.compile(f"[^'a-zA-Z]")
    result_data = filter(None, [pattern.sub("", item) for item in text.split() if item.isprintable()])

    result = [item[0] for item in collections.Counter(result_data).most_common()]
    return list(reversed(result[:3])) if len(result) >= 3 else []
