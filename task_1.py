def encrypt(text, n):
    if not text:
        return ''
    if n <= 0:
        return text
    text = text.lower()
    text = ''.join(text[1::2] + text[::2])
    return encrypt(text, n - 1)


def decrypt(encrypted_text, n):
    if not encrypted_text:
        return ''
    result = encrypted_text.lower()
    if n <= 0:
        return result
    result = ''
    odd = encrypted_text[:int(len(encrypted_text) / 2)]
    even = encrypted_text[int(len(encrypted_text) / 2):]
    for item in zip(even, odd):
        result += ''.join(item)
    if len(encrypted_text) % 2 != 0:
        result += encrypted_text[-1]
    return decrypt(result, n - 1)
