from task_1 import encrypt, decrypt
from task_2 import most_common_words

"""Пример:
("Abcdefghij", 1) -> "bdfhjacegi"
("Abcdefghij", 2) -> "bdfhjacegi" -> "dhaeibfjcg"
"""
# task 1 encrypt
print(encrypt('Abcdefghij', 1))  # -> "bdfhjacegi"
print(encrypt('Abcdefghij', 2))  # -> "dhaeibfjcg"

# task 1 decrypt

print(decrypt("bdfhjacegi", 1))  # -> "abcdefghij"
print(decrypt("dhaeibfjcg", 2))  # -> "abcdefghij"

# task 2 most_common_words

text_1 = """Contrary to popular belief, Lo'rem Ipsum is not simply random text. It has roots in a piece of classical Latin
 literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College 
 in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lo'rem Ipsum passage, and going through 
 the cites of the word in classical literature, discovered the undoubtable source. Lo'rem Ipsum comes from sections 
 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. 
 This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lo'rem Ipsum, 
 "Lo'rem ipsum dolor sit amet..", comes from a line in section 1.10.32.
"""

print(most_common_words(text_1))
